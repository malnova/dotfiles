" Couleurs des titres avec heading activé
hi RenderMarkdownH1Bg ctermfg=81 cterm=bold
hi RenderMarkdownH2Bg ctermfg=2 cterm=bold
hi RenderMarkdownH3Bg ctermfg=209 cterm=bold
hi RenderMarkdownH4Bg ctermfg=207 cterm=bold
hi RenderMarkdownH5Bg ctermfg=165 cterm=bold
hi RenderMarkdownH6Bg ctermfg=77 cterm=bold
" Couleurs des titres avec heading désactivé
hi link @markup.heading.1 RenderMarkdownH1Bg
hi link @markup.heading.2 RenderMarkdownH2Bg
hi link @markup.heading.3 RenderMarkdownH3Bg
hi link @markup.heading.4 RenderMarkdownH4Bg
hi link @markup.heading.5 RenderMarkdownH5Bg
hi link @markup.heading.6 RenderMarkdownH6Bg
" Autres
hi RenderMarkdownCode ctermfg=white ctermbg=black
hi RenderMarkdownCodeInline ctermfg=white ctermbg=black
hi RenderMarkdownLink ctermfg=39  cterm=none
hi link @markup.link RenderMarkdownLink
hi @markup.link.url ctermfg=45
hi RenderMarkdownInlineHighlight cterm=underline
hi RenderMarkdownBullet ctermfg=3 cterm=bold
hi RenderMarkdownDash ctermbg=none
hi RenderMarkdownUnchecked ctermfg=13
hi RenderMarkdownChecked ctermfg=2
hi RenderMarkdownTodo ctermfg=4
hi link @markup.list RenderMarkdownBullet
hi RenderMarkdownTableHead cterm=none
hi @markup.quote ctermfg=45
