function! s:linelight_clear() abort
    silent! call matchdelete(g:linelight['id'])
endfunction
function! s:linelight_getpos() abort
    let curline = line(".")
    if &wrap
        normal! g0
    else
        normal! 0
    endif
    let firstcol = col(".")
    if &wrap
        normal! g$
    else
        normal! $
    endif
    let lastcol = col(".") - firstcol + 1
    let g:pos += [[curline, firstcol, lastcol]]
endfunction
function! s:linelight_show() abort
    call s:linelight_clear()
    let view = winsaveview() " enregistrer la position d'origine du curseur
    let g:pos = []
    " Highlight sur la ligne courante
    call s:linelight_getpos()
    if exists('g:linelight_span') && g:linelight_span > 0
        " Highlight sur les lignes précédentes
        let c = 1
        while c <= g:linelight_span
            let c += 1
            normal! gk
            call s:linelight_getpos()
        endwhile
        " Highlight sur les lignes suivantes
        call winrestview(view) " remettre le curseur à sa position d'origine
        let c = 1
        while c <= g:linelight_span
            let c += 1
            normal! gj
            call s:linelight_getpos()
        endwhile
    endif
    let g:linelight = {}
    let g:linelight['id'] = matchaddpos("Linelight", g:pos)
    call winrestview(view) " remettre le curseur à sa position d'origine
endfunction
function! linelight#linelight_on() abort
    if exists("g:syntax_on") && exists("&syntax") | let b:synstatus = &syntax | setlocal syntax= | else | if exists("b:synstatus") | unlet b:synstatus | endif | endif
    if exists('g:linelight_dim_color')
        exe 'hi Normal ctermfg='.g:linelight_dim_color
    else
        hi Normal ctermfg=gray
    endif
    augroup linelight_plugin
        autocmd! VimResized,CursorMoved,CursorMovedI * call s:linelight_show()
    augroup END
    " Alternative avec background : hi Linelight ctermfg=white ctermbg=black
    hi Linelight ctermfg=white
    call s:linelight_show()
endfunction
function! linelight#linelight_off() abort
    call s:linelight_clear()
    augroup linelight_plugin
        autocmd!
    augroup END
    hi clear Linelight
    hi Normal ctermfg=white
    if exists("b:synstatus") | exe "setlocal syntax=".b:synstatus | endif
endfunction
