(function () {
    const instantAnimation = true;
    const returnToTop = true; // scroll back to the top when the urlbar view is closed, results are emptied, etc.
    const scrollOptions = {
        block: "nearest",
        behavior: instantAnimation ? "instant" : "auto",
    };
    const urlbarViewScroll = {
        get providersManager() {
            return (
                this._urlbarProvidersManager ||
                (this._urlbarProvidersManager =
                    gURLBar.controller.manager ||
                    ChromeUtils.import("resource:///modules/UrlbarProvidersManager.jsm")
                        .UrlbarProvidersManager)
            );
        },
    };
    function init() {
        gURLBar.view._selectElement = function (
            element,
            { updateInput = true, setAccessibleFocus = true } = {}
        ) {
            if (this._selectedElement) {
                this._selectedElement.toggleAttribute("selected", false);
                this._selectedElement.removeAttribute("aria-selected");
            }
            if (element) {
                element.toggleAttribute("selected", true);
                element.setAttribute("aria-selected", "true");
                element.scrollIntoView(scrollOptions); // ensure selected element is visible
            } else if (returnToTop)
                this._getFirstSelectableElement()?.scrollIntoView(scrollOptions); // no element is passed when this is invoked, so we probably want to scroll back to the beginning.
            this._setAccessibleFocus(setAccessibleFocus && element);
            this._selectedElement = element;

            let result = element?.closest(".urlbarView-row")?.result;
            if (updateInput) {
                this.input.setValueFromResult({
                    result,
                    urlOverride: element?.classList?.contains("urlbarView-help")
                        ? result.payload.helpUrl
                        : null,
                });
            } else this.input.setResultForCurrentValue(result);

            let provider = urlbarViewScroll.providersManager.getProvider(result?.providerName);
            if (provider) provider.tryMethod("onSelection", result, element);
        };
    }

    if (gBrowserInit.delayedStartupFinished) init();
    else {
        let delayedListener = (subject, topic) => {
            if (topic == "browser-delayed-startup-finished" && subject == window) {
                Services.obs.removeObserver(delayedListener, topic);
                init();
            }
        };
        Services.obs.addObserver(delayedListener, "browser-delayed-startup-finished");
    }
})();
