#
# ~/.bashrc
#

# Ne rien faire si le mode n'est pas interactif
[[ $- != *i* ]] && return

# Respecter la spécification XDG pour certaines applications
# (https://wiki.archlinux.org/index.php/XDG_Base_Directory_support)
# (voir aussi ~/.xinitrc)
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export KDEHOME="$XDG_CONFIG_HOME"/kde
export LESSHISTFILE=-
export LESSHISTSIZE=0

# less : afficher le nombre de lignes et le pourcentage du fichier
# dans les pages man
export MANPAGER="less +Gg"

# Revenir à la ligne au lieu de tronquer les lignes de journalctl
export SYSTEMD_LESS=FRXMK

# Supprimer les guillemets des ls
export QUOTING_STYLE=literal

# Éditeur (pour sudoedit ou sudo -e)
export EDITOR=/usr/bin/nvim

# tmux
if command -v tmux >/dev/null 2>&1 && [ "${DISPLAY}" ]; then
    if [ -z "$TMUX" ]; then
        exec /usr/local/bin/tmux
    fi
fi

# Ensemble de paramètres proches du comportement de vi (ou Vim) ;
# par ex. avec C-w les caractères spéciaux servent de séparateurs
# en plus de l'espace, comme dans vim
set -o vi

# Alias
# Penser également à vérifier dans /usr/local/bin/ si des wrappers existent
# pour certains programmes (par ex. tmux...)
alias egrep="/usr/bin/egrep --color=auto"
alias fgrep="/usr/bin/fgrep --color=auto"
alias grep="/usr/bin/grep --color=auto"
alias less="/usr/bin/less -M"
alias ls="/usr/bin/ls --color=auto"
alias units="/usr/bin/units --history $HOME/.cache/.units_history"
alias vim="/usr/bin/nvim"
alias vimdiff="/usr/bin/nvim -d"
alias sudovimdiff="SUDO_EDITOR='/usr/bin/nvim -d' sudoedit"
alias wget="/usr/bin/wget --hsts-file=$HOME/.cache/.wget-hsts"

# Prompt bash
PS1='[\u@\h \W]\$ '

# Ne pas activer le gel de OUTPUT avec Ctrl-S (sortie possible avec Ctrl-Q)
stty -ixon

# Utiliser Ctrl-L au lieu de Ctrl-V pour afficher les caractères de contrôle
stty lnext 

# Utiliser Ctrl-Backspace pour supprimer le mot précédent
bind '"": backward-kill-word'
