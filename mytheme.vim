"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" T H È M E   D ' O R I G I N E                       "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Le thème d'origine se trouve à l'emplacement suivant :
" $VIMRUNTIME/colors/peachpuff.vim
"
" Pour afficher la liste des numéros et noms de couleurs disponibles,
" utiliser la commande :h cterm-colors
" Pour afficher la liste des arguments disponibles pour cterm, utiliser
" la commande :h highlight-args
" Pour afficher la liste des groupes (highlighting groups) disponibles
" par défaut, utiliser la commande :h highlight-groups
" Pour afficher la liste de tous les groupes actuellement définis
" dans leur configuration présente, utiliser la commande :highlight

" Vim color file
" Maintainer: David Ne\v{c}as (Yeti) <yeti@physics.muni.cz>
" Last Change: 2003-04-23
" URL: http://trific.ath.cx/Ftp/vim/colors/peachpuff.vim
let colors_name = "mytheme"
hi SpecialKey term=bold ctermfg=4
hi NonText term=bold cterm=bold ctermfg=4
hi Directory term=bold ctermfg=4
hi IncSearch term=reverse cterm=reverse
hi MoreMsg term=bold ctermfg=2
hi Question term=standout ctermfg=2
hi Title term=bold ctermfg=5
hi WarningMsg term=standout ctermfg=1
hi WildMenu term=standout ctermfg=0 ctermbg=3
hi Comment term=bold ctermfg=4
hi Constant term=underline ctermfg=1
hi Special term=bold ctermfg=5
hi Statement term=bold ctermfg=3
hi PreProc term=underline ctermfg=5
hi Type term=underline ctermfg=2
hi Ignore cterm=bold ctermfg=7
hi Todo term=standout ctermfg=0 ctermbg=3

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" M O D I F I C A T I O N S   P E R S O N N E L L E S "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Couleurs différentes pour vimdiff
hi DiffChange ctermfg=white ctermbg=black
hi DiffText ctermfg=black ctermbg=darkgreen cterm=bold
hi DiffAdd ctermfg=darkgreen ctermbg=black cterm=bold
hi DiffDelete ctermfg=darkgreen ctermbg=black cterm=bold
hi clear SpellBad
hi SpellBad ctermfg=red cterm=underline
hi clear SpellCap
hi SpellCap ctermfg=white cterm=underline
hi clear SpellRare
hi SpellRare ctermfg=5 cterm=underline
hi clear SpellLocal
hi SpellLocal ctermfg=5 cterm=underline
" Couleurs différentes pour la recherche
hi Search ctermfg=black ctermbg=3
hi CurSearch ctermfg=red ctermbg=black cterm=reverse,bold
" Couleurs de la tabline avec le plugin mini.tabline
hi MiniTablineCurrent ctermfg=5 ctermbg=black cterm=underline,bold
hi MiniTablineVisible ctermfg=5 ctermbg=black cterm=none
hi link MiniTablineHidden MiniTablineVisible
hi link MiniTablineModifiedCurrent MiniTablineCurrent
hi link MiniTablineModifiedVisible MiniTablineVisible
hi link MiniTablineModifiedHidden MiniTablineVisible
hi link MiniTablineTabpagesection MiniTablineVisible
hi link MiniTablineFill MiniTablineVisible
hi link MiniTablineTrunc MiniTablineVisible
" Couleurs de la barre de statut
hi StatusLine ctermfg=black ctermbg=5 cterm=bold
hi StatusLineNC ctermfg=5 ctermbg=black cterm=none
hi GoyoStatusLine ctermfg=240 ctermbg=none cterm=none
hi link GoyoStatusLineNC GoyoStatusLine
" Surlignement des espaces surnuméraires et insécables
hi ExtraWhitespace ctermbg=DarkGray
match ExtraWhitespace /\s\+$/
hi NonBreakingSpace ctermbg=DarkGray
2match NonBreakingSpace " "
" Menu d'autocomplétion
hi Pmenu ctermfg=5 ctermbg=0
hi PmenuSel ctermfg=0 ctermbg=12 cterm=bold
" Autres changements de couleurs
hi Visual ctermfg=black ctermbg=blue
hi clear MatchParen
hi MatchParen ctermfg=white ctermbg=DarkGray
hi Error ctermfg=black ctermbg=red cterm=bold
hi ErrorMsg ctermfg=black ctermbg=red cterm=bold
hi LineNr ctermfg=5 ctermbg=black cterm=none
hi CursorLineNr ctermfg=white ctermbg=black cterm=bold
hi FoldColumn ctermfg=white ctermbg=black cterm=bold
hi VertSplit ctermfg=black ctermbg=none cterm=none
hi Folded ctermfg=black ctermbg=green cterm=bold
hi ModeMsg ctermfg=white
hi ColorColumn ctermfg=black
hi Identifier ctermfg=6 cterm=none
hi EndOfBuffer ctermfg=darkgrey
