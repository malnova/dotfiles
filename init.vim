"------------------------------------------------------------
" Plugins

" Plugin vim-plug (https://github.com/junegunn/vim-plug) pour
" installer facilement les plugins et les thèmes
" Commandes :
" :PlugInstall : installer les plugins
" :PlugUpgrade : mettre à jour vim-plug
" :PlugStatus : vérifier si des mises à jour des plugins sont disponibles
" :PlugUpdate : mettre à jour les plugins
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Liste des plugins à installer automatiquement avec vim-plug
call plug#begin('~/.config/nvim/bundle')
    " Plugin mini.tabline, pour afficher la liste des tabs et des buffers
    " dans la tabline :
    Plug 'https://github.com/echasnovski/mini.tabline'

    " Plugin Goyo, pour écrire sans distraction (en masquant les différentes
    " barres, horizontales et verticales, de vim et de tmux) :
    Plug 'https://github.com/junegunn/goyo.vim', { 'on': 'Goyo' }

    " Plugin Vim Tmux Navigator, pour pouvoir utiliser les mêmes raccourcis
    " pour passer d'un panneau (split) à l'autre dans vim et dans Tmux
    Plug 'https://github.com/christoomey/vim-tmux-navigator'

    " Plugin (Better) Vim Tmux Resizer, pour pouvoir utiliser les mêmes
    " raccourcis pour redimensionner un panneau dans vim et dans Tmux
    Plug 'https://github.com/RyanMillerC/better-vim-tmux-resizer'

    " Plugin render-markdown.nvim, pour prévisualiser le markdown
    " Dépendance nécessaire : sudo pacman -S ttf-nerd-fonts-symbols-mono
    " (installe aussi ttf-nerd-fonts-symbols-common)
    Plug 'https://github.com/MeanderingProgrammer/render-markdown.nvim', { 'for' : 'markdown' }
    Plug 'https://github.com/nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate', 'for' : 'markdown' }
    Plug 'https://github.com/echasnovski/mini.icons'
call plug#end()

"------------------------------------------------------------
" Ne pas montrer le message d'intro ("welcome screen", visible avec :intro)
set shortmess+=I

"------------------------------------------------------------
" Choix du thème
" On peut voir et appliquer les thèmes déjà installés en tapant
" :colo (ou :colorscheme) puis <Tab> jusqu'au thème voulu et <Entrée>
" Les thèmes par défaut (ceux installés en même temps que neovim)
" se trouvent dans $VIMRUNTIME/colors/
silent! colorscheme mytheme

"------------------------------------------------------------
" Options de base

" Recherche insensible à la casse, sauf si elle contient des majuscules
set ignorecase
set smartcase

" Demander à enregistrer le fichier au lieu d'afficher une erreur
" en cas de modification non enregistrée
set confirm

" Avertissement visuel au lieu de beep en cas de problème
set visualbell

"------------------------------------------------------------
" Options d'indentation
set tabstop=4 " Largeur d'une tabulation
set softtabstop=4 " Ignoré avec expandtab, sauf pour un delete dans une ligne
set expandtab " Espaces au lieu des tabulations
set shiftwidth=4 " Largeur de l'indentation (<< ou >> en modes normal et visuel)

"------------------------------------------------------------
" Autres options utiles
set breakindent breakindentopt=shift:1,sbr " Décalage des lignes en cas de wrap
set linebreak " Ne pas revenir à la ligne au milieu d'un mot
set scrolloff=2 " Minimum de 3 lignes autour du curseur en cas de scroll
set noincsearch " Pas de recherche pendant la frappe
set foldcolumn=auto " Afficher / masquer automatiquement la barre pour les folds (replis)

"------------------------------------------------------------
" Format de la barre de statut
" %F : chemin complet et nom du fichier
" [%{strlen(&fenc)?&fenc:'Aucun encodage'}] : encodage du fichier
" %m : signal (flag) si fichier modifié
" %= : aligné à droite
" %l/%L, : ligne X de Y
" %c : colonne actuelle
" [%p%%] : [pourcentage du fichier]
set statusline=%F\ [%{strlen(&fenc)?&fenc:'Aucun\ encodage'}]\ %m%=%l/%L,%c\ [%p%%]
augroup StatusHighlight
    autocmd!
    autocmd FocusLost,WinLeave * setlocal winhighlight+=StatusLine:StatusLineNC
    autocmd FocusGained,WinEnter * setlocal winhighlight-=StatusLine:StatusLineNC
augroup END

"------------------------------------------------------------
" Numéros de lignes relatifs + activer cursorline pour permettre
" le highlight sur CursorLineNr dans le thème
set number
set relativenumber
set cursorline
set cursorlineopt=number

"------------------------------------------------------------
" Ouvrir les splits en bas et à droite par défaut
set splitright
set splitbelow

"------------------------------------------------------------
" Redimensionnement automatique des splits
augroup resizeEqualWindows
    autocmd! VimResized * wincmd =
augroup END

"------------------------------------------------------------
" Ligne précédente / suivante pour touches gauches / droites
silent! set whichwrap+=<,>,h,l,[,]

"------------------------------------------------------------
" Touche home pour début du texte OU de la ligne en cas de wrap
nnoremap <silent> <Home> :call SmartHome("n")<CR>
inoremap <silent> <Home> <C-r>=SmartHome("i")<CR>
vnoremap <silent> <Home> <Esc>:call SmartHome("v")<CR>
function! SaveMark(...) abort
    let l:name = a:0 ? a:1 : 'm'
    let s:save_mark = getpos("'".l:name)
endfunction
function! RestoreMark(...) abort
    let l:name = a:0 ? a:1 : 'm'
    call setpos("'".l:name, s:save_mark)
endfunction
function! SmartHome(mode) abort
    let curcol = col(".")
    if curcol > indent(".") + 2
        call cursor(0, curcol - 1)
    endif
    if curcol == 1 || curcol > indent(".") + 1
        if &wrap
            normal! g^
        else
            normal! ^
        endif
    else
        if &wrap
            normal! g0
        else
            normal! 0
        endif
    endif
    if a:mode == "v"
        call SaveMark('m')
        call setpos("'m", getpos('.'))
        normal! gv`m
        call RestoreMark('m')
    endif
    return ""
endfunction

"------------------------------------------------------------
" Touche end pour fin du texte OU de la ligne en cas de wrap
nnoremap <silent> <End> :call SmartEnd("n")<CR>
inoremap <silent> <End> <C-r>=SmartEnd("i")<CR>
vnoremap <silent> <End> <Esc>:call SmartEnd("v")<CR>
function! SaveReg(...) abort
    let l:name = a:0 ? a:1 : v:register
    let s:save_reg = [getreg(l:name), getregtype(l:name)]
endfunction
function! RestoreReg(...) abort
    let l:name = a:0 ? a:1 : v:register
    if exists("s:save_reg")
        call setreg(l:name, s:save_reg[0], s:save_reg[1])
    endif
endfunction
function! SmartEnd(mode) abort
    let curcol = col(".")
    let lastcol = a:mode == "i" ? col("$") : col("$") - 1
    if curcol < lastcol - 1
        call SaveReg()
        normal! yl
        let l:charlen = byteidx(getreg(), 1)
        call cursor(0, curcol + l:charlen)
        call RestoreReg()
    endif
    if curcol < lastcol
        if &wrap
            normal! g$
        else
            normal! $
        endif
    else
        normal! g_
    endif
    if a:mode == "i"
        call SaveReg()
        normal! yl
        let l:charlen = byteidx(getreg(), 1)
        call cursor(0, col(".") + l:charlen)
        call RestoreReg()
    endif
    if a:mode == "v"
        call SaveMark('m')
        call setpos("'m", getpos('.'))
        normal! gv`m
        call RestoreMark('m')
    endif
    return ""
endfunction

"------------------------------------------------------------
" Ctrl-Home doit aussi déplacer le curseur en début de ligne
noremap <C-Home> <C-Home>0<Ignore>

"------------------------------------------------------------
" Comportement normal des touches de direction (ligne par ligne
" en cas de wrap)
noremap <Down> gj<Ignore>
noremap <Up> gk<Ignore>

"------------------------------------------------------------
" Déplacement de l'écran avec Ctrl-flèches haut / bas
noremap <C-Down> <C-e><Ignore>
noremap <C-Up> <C-y><Ignore>
inoremap <C-Down> <C-o><C-e><Ignore>
inoremap <C-Up> <C-o><C-y><Ignore>

"------------------------------------------------------------
" Déplacement au mot précédent / suivant avec Ctrl-flèches gauche / droite
noremap <C-Left> hb<Ignore>
noremap <C-Right> el<Ignore>
inoremap <expr> <silent> <C-Left> '<C-o>:normal! hb<CR>'
inoremap <expr> <silent> <C-Right> '<C-o>:normal! el<CR>'

"------------------------------------------------------------
" Suppression du mot précédent / suivant avec Ctrl-Backspace et Ctrl-Suppr
inoremap <expr> <silent>  col('.') <= 2 ? '<C-w>' : col('.') >= col('$') ? '<C-o>:normal! bd$<CR>' : '<C-o>:normal! m"hbdg`"<CR>'
inoremap <expr> <silent> <C-Del> col('.') >= col('$') - 1 ? '<Del>' : '<C-o>de'

"------------------------------------------------------------
" Mêmes délimiteurs que dans bash : _ est aussi un délimiteur
set iskeyword-=_

"------------------------------------------------------------
" Comportement normal de la touche Suppr en mode commande (ne pas supprimer
" les caractères précédents quand il n'y a plus de caractères suivants)
cnoremap <expr> <Del> getcmdpos() <= strlen(getcmdline()) ? "\<Del>" : ""

"------------------------------------------------------------
" Comportement normal des touches PageUp et PageDown (saut d'1 page et
" jusqu'en haut / en bas du fichier)
noremap <PageUp> 1000<C-u><Ignore>
noremap <PageDown> 1000<C-d><Ignore>

"------------------------------------------------------------
" Paragraphe précédent / suivant avec Ctrl-PageUp / PageDown
noremap <C-PageUp> {<Ignore>
noremap <C-PageDown> }<Ignore>
inoremap <C-PageUp> <C-o>{<Ignore>
inoremap <C-PageDown> <C-o>}<Ignore>

"------------------------------------------------------------
" Pouvoir insérer une espace avec la barre d'espace en mode normal
nnoremap <Space> i<Space><Right><Esc>

"------------------------------------------------------------
" Copier le registre courant " (unnamed register) avec Ctrl-V
inoremap <C-V> <C-r>"

"------------------------------------------------------------
" Caractères de contrôle avec Ctrl-l comme dans bash
inoremap <C-l> <C-v>

"------------------------------------------------------------
" Remplacer le délimiteur de fenêtre (split) verticale par une espace
set fillchars+=vert:\ 

"------------------------------------------------------------
" Buffer / onglet (tab) suivant avec C-b / C-t
nnoremap <C-b> :bnext<CR>
nnoremap <C-t> :tabnext<CR>

"------------------------------------------------------------
" Diviser les fenêtres horizontalement et verticalement
" Imite les raccourcis de tmux
nnoremap <C-w>- <C-w>s
nnoremap <C-w>\| <C-w>v
nnoremap <C-w>_ :botright split<CR>
nnoremap <C-w>\ :botright vsplit<CR>

"------------------------------------------------------------
" Correcteur d'orthographe (spellcheck)
" Activer / désactiver le correcteur
nnoremap <silent> zs :setlocal spell! spell? spelllang=fr,en<CR>
" Erreur précédente / suivante
nnoremap zN [s
nnoremap zn ]s

"------------------------------------------------------------
" Menu d'autocomplétion
cnoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"
cnoremap <expr> <Up> pumvisible() ? "\<C-p>" : "\<Up>"
cnoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<C-c>"
cnoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <silent> <Down> pumvisible() ? "\<C-n>" : "\<C-o>gj"
inoremap <expr> <silent> <Up> pumvisible() ? "\<C-p>" : "\<C-o>gk"
inoremap <expr> <silent> <PageUp> pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-o>1000\<C-u>"
inoremap <expr> <silent> <PageDown> pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-o>1000\<C-d>"
inoremap <expr> <silent> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <silent> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Afficher le menu même lorsqu'il n'y a qu'une seule possibilité
" d'autocomplétion
set completeopt=menuone

" C-c pour désactiver / réactiver le surlignement lors d'une recherche
nnoremap <silent> <C-c> :let v:hlsearch=(&hls && !v:hlsearch)<CR><C-L>

"------------------------------------------------------------
" Mode diff
" Calculer le nombre de différences
function! DiffCount() abort
    setlocal nocursorbind
    setlocal noscrollbind
    let winview = winsaveview()
    let pos = getpos(".")
    silent exe 'normal! gg'
    let moved = 1
    let hunks = []
    while moved
        let startl = line(".")
        keepjumps silent exe 'normal! ]c'
        let moved = line(".") - startl
        if moved
            call add(hunks,line("."))
        endif
    endwhile
    call winrestview(winview)
    call setpos(".",pos)
    setlocal cursorbind
    setlocal scrollbind
    return len(hunks) > 0 ? ' [' . len(hunks) . ' diff]' : ''
endfunction
" Pas de coloration syntaxique ni de correcteur d'orthographe
function! Set_diff_mode_options() abort
    windo let b:diffstatus = 1 | if luaeval("vim.treesitter.highlighter.active[vim.api.nvim_get_current_buf()] ~= nil") | let b:treesitterstatus = 1 | exe "RenderMarkdown buf_disable" | exe "TSBufDisable highlight" | endif | if exists("&syntax") | let b:synstatus = &syntax | endif | setlocal syntax= | if &spell | let b:spellstatus = &spell | endif | setlocal nospell
    wincmd t
endfunction
function! Set_diff_mode_options_when_diffthis() abort
    if &diff
        if !exists("b:diffstatus")
            let b:diffstatus = 1
            if luaeval("vim.treesitter.highlighter.active[vim.api.nvim_get_current_buf()] ~= nil")
                let b:treesitterstatus = 1
                exe "RenderMarkdown buf_disable"
                exe "TSBufDisable highlight"
            else
                if exists("b:treesitterstatus")
                    unlet b:treesitterstatus
                endif
            endif
            if exists("&syntax")
                let b:synstatus = &syntax
                setlocal syntax=
            else
                if exists("b:synstatus")
                    unlet b:synstatus
                endif
            endif
            if &spell
                let b:spellstatus = &spell
                setlocal nospell
            else
                if exists("b:spellstatus")
                    unlet b:spellstatus
                endif
            endif
        endif
    else
        if exists("b:diffstatus")
            unlet b:diffstatus
            if exists("b:treesitterstatus")
                exe "RenderMarkdown buf_enable"
                exe "TSBufEnable highlight"
            endif
            if exists("b:synstatus")
                exe "setlocal syntax=".b:synstatus
            endif
            if exists("b:spellstatus")
                let &spell = b:spellstatus
            endif
        endif
    endif
endfunction
augroup diffMode
    autocmd!
    autocmd VimEnter * if &diff | call Set_diff_mode_options() | endif
    autocmd OptionSet diff call Set_diff_mode_options_when_diffthis()
    autocmd DiffUpdated * call DiffCount()
augroup END
" Algorithme plus juste et nombre de lignes réduit autour des lignes
" comportant des différences (par défaut : 6)
set diffopt+=algorithm:patience,linematch:50,context:2
" Différence précédente / suivante
nnoremap dN [c
nnoremap dn ]c
" Obtenir / partager les changements sur la ligne du curseur seulement
nnoremap dO V:diffget<CR>
nnoremap dP V:diffput<CR>

"------------------------------------------------------------
" Permettre l'enregistrement des fichiers en lecture seule avec Polkit
autocmd BufEnter * set noro " Ne pas avertir que le fichier est en lecture seule
function! Save_with_Polkit() abort
    if &diff | set eventignore=DiffUpdated | endif
    silent exe 'w !pkexec tee %:p > /dev/null'
    e!
    if &diff | set eventignore= | endif
endfunction
command! -bang W call Save_with_Polkit()
command! -bang Wq call Save_with_Polkit() | q
command! -bang Wqa call Save_with_Polkit() | qa

"------------------------------------------------------------
" Plugin mini.tabline
if has_key(g:plugs, "mini.tabline")
    set list
    set listchars=tab:>\ ,precedes:<,extends:>
lua << EOF
    require('mini.tabline').setup({
        format = function(buf_id, label)
            local suffix = vim.bo[buf_id].modified and '*' or ''
            if vim.wo.diff and buf_id == vim.api.nvim_get_current_buf() then
                local diff_count = vim.fn.DiffCount()
                suffix = suffix .. diff_count
            end
            if vim.api.nvim_buf_get_name(buf_id) == '' then return ' [Aucun nom]' .. suffix .. ' ' end
            return ' ' .. label .. suffix .. ' '
        end,
        tabpage_section = 'right',
    })
EOF
endif

"------------------------------------------------------------
" Plugin Goyo
if has_key(g:plugs, "goyo.vim")
    function! SetGoyoStatus() abort
        setlocal statusline=%=%l/%L\ [%p%%]
    endfunction
    function! s:goyo_enter() abort
        if executable('tmux') && strlen($TMUX)
            silent !tmux set status off
            silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
        endif
        call SetGoyoStatus()
        set noshowmode
        set noshowcmd
        let b:scrolloff = &scrolloff
        setlocal scrolloff=999
        LinelightOn
    endfunction
    function! s:goyo_leave() abort
        if executable('tmux') && strlen($TMUX)
            silent !tmux set status on
            silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
        endif
        set showmode
        set showcmd
        if exists("b:scrolloff") | exe "setlocal scrolloff=".b:scrolloff | endif
        LinelightOff
    endfunction
    augroup GoyoSettings
        autocmd!
        autocmd User GoyoEnter nested call <SID>goyo_enter()
        autocmd User GoyoLeave nested call <SID>goyo_leave()
    augroup END
endif

"------------------------------------------------------------
" Plugin Vim Tmux Navigator
if has_key(g:plugs, "vim-tmux-navigator")
    let g:tmux_navigator_no_mappings = 1
    nnoremap <silent> <S-Left> :TmuxNavigateLeft<CR>
    nnoremap <silent> <S-Down> :TmuxNavigateDown<CR>
    nnoremap <silent> <S-Up> :TmuxNavigateUp<CR>
    nnoremap <silent> <S-Right> :TmuxNavigateRight<CR>
    inoremap <silent> <S-Left> <C-o>:TmuxNavigateLeft<CR>
    inoremap <silent> <S-Down> <C-o>:TmuxNavigateDown<CR>
    inoremap <silent> <S-Up> <C-o>:TmuxNavigateUp<CR>
    inoremap <silent> <S-Right> <C-o>:TmuxNavigateRight<CR>
endif

"------------------------------------------------------------
" Plugin (Better) Vim Tmux Resizer
if has_key(g:plugs, "better-vim-tmux-resizer")
    let g:tmux_resizer_no_mappings = 1
    " Incrémentation horizontale
    let g:tmux_resizer_resize_count = 2
    " Incrémentation verticale
    let g:tmux_resizer_vertical_resize_count = 2
    nnoremap <silent> <M-Left> :TmuxResizeLeft<CR>
    nnoremap <silent> <M-Down> :TmuxResizeDown<CR>
    nnoremap <silent> <M-Up> :TmuxResizeUp<CR>
    nnoremap <silent> <M-Right> :TmuxResizeRight<CR>
    inoremap <silent> <M-Left> <C-o>:TmuxResizeLeft<CR>
    inoremap <silent> <M-Down> <C-o>:TmuxResizeDown<CR>
    inoremap <silent> <M-Up> <C-o>:TmuxResizeUp<CR>
    inoremap <silent> <M-Right> <C-o>:TmuxResizeRight<CR>
endif

"------------------------------------------------------------
" Plugin render-markdown.nvim
if has_key(g:plugs, "render-markdown.nvim") && has_key(g:plugs, "nvim-treesitter")
    function! SetRenderMarkdownOptions() abort
        if exists("g:plug_treesitter_loaded") | return | endif
        let g:plug_treesitter_loaded = 1
lua << EOF
    local function find_all(buf, node, pattern)
        local start_row, _, end_row, _ = node:range()
        end_row = end_row + (start_row == end_row and 1 or 0)
        local lines = vim.api.nvim_buf_get_lines(buf, start_row, end_row, false)
        local result = {}
        for row, line in ipairs(lines) do
            local index = 1
            while index ~= nil do
                local start_index, end_index = line:find(pattern, index)
                if start_index == nil or end_index == nil then
                    index = nil
                else
                    table.insert(result, {
                        row = start_row + row - 1,
                        col = { start_index - 1, end_index },
                    })
                    index = end_index + 1
                end
            end
        end
        return result
    end
    local function mark(match, text)
        return {
            conceal = true,
            start_row = match.row,
            start_col = match.col[1],
            opts = {
                end_col = match.col[2],
                conceal = '',
                virt_text = { { text, 'DiagnosticOk' } },
                virt_text_pos = 'inline',
            },
        }
    end
    local function render_dashes(ctx)
        local query = vim.treesitter.query.parse(
            'markdown_inline',
            '((inline) @replacements (#lua-match? @replacements "[%.%-][%.%-]"))'
        )
        local result = {}
        for _, node in query:iter_captures(ctx.root, ctx.buf) do
            local _, start_col, _, _ = node:range()
            if start_col == 0 then
                local ellipses = find_all(ctx.buf, node, '%.%.%.+')
                for _, ellipse in ipairs(ellipses) do
                    table.insert(result, mark(ellipse, '…'))
                end
                local dashes = find_all(ctx.buf, node, '%-%-+')
                for _, dash in ipairs(dashes) do
                    local width = dash.col[2] - dash.col[1]
                    local ems = math.floor(width / 3)
                    width = math.fmod(width, 3)
                    local ens = math.floor(width / 2)
                    width = math.fmod(width, 2)
                    local text = string.rep('—', ems) .. string.rep('–', ens) .. string.rep('-', width)
                    table.insert(result, mark(dash, text))
                end
            end
        end
        return result
    end
    require("render-markdown").setup({
        debounce = 50,
        log_level = 'off',
        latex = { enabled = false },
        heading = { sign = false, icons = { '' }, left_pad = { 0.5, 0 } },
        code = { sign = false },
        bullet = { icons = { '•', '◦', '▪' } },
        quote = { icon = '┃' },
        pipe_table = { style = 'normal' },
        custom_handlers = { markdown_inline = { extends = true, parse = render_dashes }, },
    })
    require('nvim-treesitter.configs').setup({
        ensure_installed = { 'markdown', 'markdown_inline' },
        highlight = { enable = true },
    })
EOF
    endfunction
    augroup RenderMarkdownSettings
        autocmd! Filetype markdown call SetRenderMarkdownOptions()
    augroup END
endif

"------------------------------------------------------------
" Commandes pour convertir en markdown, odt, doc, etc.
" Les noms de commandes doivent commencer par une majuscule
command! ConvToDoc call convertfiles#convert("doc", "1", "0")
command! ConvToDocx call convertfiles#convert("docx", "1", "0")
command! ConvToHtml call convertfiles#convert("html", "0", "0")
command! ConvToOdt call convertfiles#convert("odt", "0", "0")
command! ConvToPdf call convertfiles#convert("pdf", "1", "0")
command! ConvToRtf call convertfiles#convert("rtf", "1", "0")
command! Prev call convertfiles#convert("pdf", "1", "1")
command! -complete=file -nargs=+ ConvToMd call convertfiles#convert_to_text("1", <f-args>)
command! -complete=file -nargs=+ ConvToTxt call convertfiles#convert_to_text("0", <f-args>)
command! -complete=customlist,convertfiles#CompletionTest -nargs=* ConvToSlides call convertfiles#convert_to_slides(<f-args>)

"------------------------------------------------------------
" Commandes pour définir un highlight sur les lignes entourant le curseur
let g:linelight_span = 3 " Nombre de lignes autour du curseur (par défaut: 0)
let g:linelight_dim_color = 243 " Couleur pour le reste du texte (par défaut: gray)
command! LinelightOn call linelight#linelight_on()
command! LinelightOff call linelight#linelight_off()
